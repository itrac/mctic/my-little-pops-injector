package injector;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Injector {

	private final String CONTEXT;
	private Logger logger;
	private Robot robot;
	private StateMachine stateMachine;

	private JSONObject data;
	private ArrayList<String> succeededPOPs;
	private ArrayList<String> failedPOPs;

	public Injector() {
		CONTEXT = "Injector";
		final String INFORMATION = " to create Injector";

		stateMachine = new StateMachine();
		succeededPOPs = new ArrayList<>();
		failedPOPs = new ArrayList<>();

		try {
			data = getPops("<FILENAME>");

			logger = new Logger(stateMachine);

			robot = new Robot(logger);

			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			System.out.println(e.getMessage());
			stateMachine.changeToFail();
		}

	};

	public void inject() {
		if (!stateMachine.isInjectionFailed()) {
			int attempts = 0;
			do {
				attempts += 1;
				logger.inform(CONTEXT, "Attempt " + attempts + " to inject STARTED");
				stateMachine.changeToSafe();

				loginToServer();

				if (!stateMachine.isInjectionFailed()) {
					performInjections();
				}
			} while (attempts < 2 && stateMachine.isInjectionFailed());
		}
	}

	public void loginToServer() {
		LoginManager loginManager = new LoginManager(logger, robot);
		int attempts = 0;
		do {
			attempts += 1;
			stateMachine.changeToSafe();
			try {
				loginManager.performLogin();
				logger.inform(CONTEXT, "Succeeded to perform Login");
			} catch (Exception e) {
				stateMachine.changeToFail();
				String information = "Attempt " + attempts + " to Login FAILED";
				logger.error(CONTEXT, information);
				System.out.println(e.getMessage());
			}
		} while (attempts < 2 && stateMachine.isInjectionFailed());
	}

	public void performInjections() {
		final String INFORMATION = " to inject POP";
		try {
			JSONArray pops = (JSONArray) data.get("pops");

			KnowledgeManager knowledgeManager = new KnowledgeManager(logger, robot);

			for (Object object : pops) {
				JSONObject pop = (JSONObject) object;
				String id = (String) pop.get("id");
				try {
					if (!succeededPOPs.contains(id)) {
						knowledgeManager.performInjection(data, pop);
						logger.inform(CONTEXT, "Succeeded" + INFORMATION);
						succeededPOPs.add(id);
					} else {
						logger.inform(CONTEXT, "POP already injected");
					}

				} catch (Exception e) {
					logger.error(CONTEXT, "Failed" + INFORMATION);
					System.out.println(e.getMessage());
					if (!failedPOPs.contains(id)) {
						failedPOPs.add(id);
					}
				}
			}
		} catch (Exception e2) {
			logger.error(CONTEXT, "Failed to get POP(s)");
			System.out.println(e2.getMessage());
			stateMachine.changeToFail();
		}
	}

	@SuppressWarnings("unchecked")
	public void report() {
		JSONObject report = new JSONObject();
		report.put("total", succeededPOPs.size() + failedPOPs.size());
		report.put("succeeded", succeededPOPs.size());
		report.put("failed", failedPOPs.size());

		JSONArray succeeded = new JSONArray();
		succeeded.addAll(succeededPOPs);
		report.put("succeededPOPs", succeeded);

		JSONArray failed = new JSONArray();
		failed.addAll(failedPOPs);
		report.put("failedPOPs", failed);

		logger.result(CONTEXT, report.toJSONString());
	}

	public void shutdown() {
		robot.shutdown();
		logger.closeFiles();
	}

	private JSONObject getPops(String file) {
		JSONParser parser = new JSONParser();
		try {
			return (JSONObject) parser.parse(new FileReader("/home/arkye/Workspace/injector/data/" + file + ".json"));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
