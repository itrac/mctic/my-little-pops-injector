package injector;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Robot {

	private String context;
	private Logger logger;

	private WebDriver driver;
	private WebElement current;
	private WebDriverWait wait;
	private Actions actions;

	@SuppressWarnings("deprecation")
	public Robot(Logger logger) throws Exception {
		this.context = "Robot";
		final String INFORMATION = " to create Robot";

		this.logger = logger;

		try {
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("incognito");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);

			driver = new ChromeDriver(capabilities);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, 10);
			actions = new Actions(driver);

			logger.inform(context, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public void shutdown() {
		driver.close();
	}

	public void open(String path) throws Exception {
		final String INFORMATION = " to open PATH (" + path + ")";
		try {
			driver.get(path);

			logger.inform(context, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathExists(String xpath) throws Exception {
		final String INFORMATION = " to find XPATH (" + xpath + ")";
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			current = driver.findElement(By.xpath(xpath));

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathExistChild(String xpath) throws Exception {
		final String INFORMATION = " to find child XPATH (" + xpath + ")";
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			current = current.findElement(By.xpath(xpath));

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean click() throws Exception {
		final String INFORMATION = " to perform a click in current Element";
		try {
			current.click();
			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathClick(String xpath) throws Exception {
		final String INFORMATION = " to click in XPATH (" + xpath + ")";
		try {
			if (xpathExists(xpath)) {
				current.click();
			}

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathClickOnChild(String xpath) throws Exception {
		final String INFORMATION = " to click in child XPATH (" + xpath + ")";
		try {
			if (xpathExistChild(xpath)) {
				current.click();
			}

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathInsert(String xpath, String value) throws Exception {
		final String INFORMATION = " to insert VALUE (" + value + ") in XPATH (" + xpath + ")";
		try {
			if (xpathExists(xpath)) {
				current.sendKeys(value);
			}

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathInsert(String xpath, String value, InsertionInterface insertionInterface) throws Exception {
		final String INFORMATION = " to insert with Interface";
		try {
			if (xpathExists(xpath)) {
				if (insertionInterface.beforeInsertion(this)) {
					xpathInsert(xpath, value);
					if (insertionInterface.afterInsertion(this)) {
						logger.inform(context, "Succeeded" + INFORMATION);
					} else {
						throw new Exception("Condition afterInsertion not satisfied");
					}
				} else {
					throw new Exception("Condition beforeInsertion not satisfied");
				}
			}
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathSelect(String xpath, String value) throws Exception {
		final String INFORMATION = " to select VALUE (" + value + ") in XPATH (" + xpath + ")";
		try {
			if (xpathExists(xpath)) {
				Select select = new Select(current);
				select.selectByVisibleText(value);
			}

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean xpathExecuteJavaScript(String xpath, String js) throws Exception {
		final String INFORMATION = " to execute JS (" + js + ") in XPATH (" + xpath + ")";
		try {
			if (xpathExists(xpath)) {
				current.clear();
				((JavascriptExecutor) driver).executeScript(js, current);
			}

			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}

	public boolean moveToCurrent() throws Exception {
		final String INFORMATION = " to move to current";
		try {
			actions.moveToElement(current).perform();
			logger.inform(context, "Succeeded" + INFORMATION);
			return true;
		} catch (Exception e) {
			logger.error(context, "Failed" + INFORMATION);
			throw e;
		}
	}
}
