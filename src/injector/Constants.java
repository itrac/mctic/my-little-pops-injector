package injector;

public class Constants {

	public static final boolean IS_PRODUCTION = true;

	public static final String LOGIN_DEVELOPMENT_URL = "http://atendimento.hml.mctic.gov.br/citsmart/pages/login/login.load";
	public static final String LOGIN_PRODUCTION_URL = "http://atendimento.mctic.gov.br/citsmart/pages/login/login.load";

	public static final String LOGIN_FORM_XPATH = xpath("form", "id", "formlogin");
	public static final String LOGIN_INPUT_USERNAME_XPATH = xpath("input", "name", "user");
	public static final String LOGIN_INPUT_USERNAME = "<USERNAME>";
	public static final String LOGIN_INPUT_PASSWORD_XPATH = xpath("input", "name", "senha");
	public static final String LOGIN_INPUT_PASSWORD = "<PASSWORD>";
	public static final String LOGIN_BUTTON_XPATH = xpath("button", "onclick", "LOGIN.validation();");

	public static final String PORTAL_INPUT_SEARCH = xpath("input", "id", "pesquisaPortal");

	public static final String NEW_KNOWLEDGE_DEVELOPMENT_URL = "http://atendimento.hml.mctic.gov.br/citsmart/pages/knowledgeBase/knowledgeBase.load#/knowledge/new/";
	public static final String NEW_KNOWLEDGE_PRODUCTION_URL = "http://atendimento.mctic.gov.br/citsmart/pages/knowledgeBase/knowledgeBase.load#/knowledge/new/";

	public static final String KNOWLEDGE_INPUT_TITLE_XPATH = xpath("input", "id", "knowledge.title");
	public static final String KNOWLEDGE_SELECT_FOLDER_XPATH = xpath("select", "ng-options", "folder.id as folder.nome for folder in list ");
	public static final String KNOWLEDGE_SELECT_SOURCE_XPATH = xpath("select", "ng-options", "source.id*1 as source.description for source in list ");
	public static final String KNOWLEDGE_SELECT_SITUATION_XPATH = xpath("select", "ng-options", "situation.id as (situation.I18NKey | translate) for situation in list ");
	public static final String KNOWLEDGE_INPUT_EXPIRATION_DATE_XPATH = xpath("input", "name", "knowledge.dateExpiration");
	public static final String KNOWLEDGE_BUTTON_DATE_PICKER_CLOSE_XPATH = xpath("button", "ng-click", "close($event)");
	public static final String KNOWLEDGE_SELECT_PRIVACY_XPATH = xpath("select", "ng-options", "privacy.id as (privacy.I18NKey | translate) for privacy in list ");
	public static final String KNOWLEDGE_TOGGLE_PUBLISH_XPATH = xpath("toggle", "id", "knowledge-isPublicar");
	public static final String KNOWLEDGE_LINK_SOURCE_CODE_XPATH = xpath("a", "id", "cke_18");
	public static final String KNOWLEDGE_TEXTAREA_XPATH = xpath("textarea", "dir", "ltr");

	public static final String KNOWLEDGE_SAVE_DIV = xpath("div", "ng-drag-data", "{obj}");
	public static final String KNOWLEDGE_SAVE_ICON_DIV = xpath("div", "icon", "save");
	public static final String KNOWLEDGE_SAVE_BUTTON = xpath("button", "class", "btn btn-citsmart btn-floating btn-floating-show-animate");

	public static final String KNOWLEDGE_SUCCESS_ALERT_DIV = xpath("div", "class", "alert alert-success alert-dismissible");

	private static final String xpath(String tag, String attribute, String value) {
		return "//" + tag + "[@" + attribute + "='" + value + "']";
	}
}
