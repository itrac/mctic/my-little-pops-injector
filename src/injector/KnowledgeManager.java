package injector;

import org.json.simple.JSONObject;

public class KnowledgeManager {

	private final String CONTEXT;

	private Logger logger;
	private Robot robot;

	public KnowledgeManager(Logger logger, Robot robot) {
		CONTEXT = "Knowledge";

		this.logger = logger;
		this.robot = robot;
	}

	public void performInjection(JSONObject data, JSONObject pop) throws Exception {
		final String INFORMATION = " to insert POP (" + pop.get("number") + ")";
		try {
			openPage();
			if (robot.xpathExists(Constants.KNOWLEDGE_INPUT_TITLE_XPATH)) {
				insertTitle(pop);
				selectFolder(pop);
				selectSource();
				selectSituation();
				insertExpirationDate();
				selectPrivacy();
				togglePublish();
				changeToSourceCode();
				insertProcedure(data, pop);
				submit();
				if (robot.xpathExists(Constants.KNOWLEDGE_SUCCESS_ALERT_DIV)) {
					logger.success(CONTEXT, "Succeeded" + INFORMATION);
				} else {
					throw new Exception("After submit, success alert not found");
				}
			} else {
				throw new Exception("Knowledge form not found");
			}
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void openPage() throws Exception {
		final String INFORMATION = " to open Knowledge Page";
		try {
			if (Constants.IS_PRODUCTION) {
				robot.open(Constants.NEW_KNOWLEDGE_PRODUCTION_URL);
			} else {
				robot.open(Constants.NEW_KNOWLEDGE_DEVELOPMENT_URL);
			}
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void insertTitle(JSONObject pop) throws Exception {
		final String INFORMATION = " to insert Title";
		try {
			String title = getTitle(pop);
			robot.xpathInsert(Constants.KNOWLEDGE_INPUT_TITLE_XPATH, title);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private String getTitle(JSONObject pop) throws Exception {
		JSONObject fields = (JSONObject) pop.get("fields");
		String newPopName = (String) fields.get("newPopName");
		if (newPopName != null && newPopName.length() > 5
				&& !newPopName.contains("Valor para")) {
			return newPopName;
		} else {
			String title = (String) pop.get("name");
			return title;
		}
	}

	private void selectFolder(JSONObject pop) throws Exception {
		final String INFORMATION = " to select Folder";
		try {
			String folder = ((String) ((JSONObject) pop.get("level")).get("name")).equals("N2") ? "....Suporte"
					: "....Infraestrutura";
			robot.xpathSelect(Constants.KNOWLEDGE_SELECT_FOLDER_XPATH, folder);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void selectSource() throws Exception {
		final String SOURCE = "Conhecimento";
		final String INFORMATION = " to select Source as " + SOURCE;
		try {
			robot.xpathSelect(Constants.KNOWLEDGE_SELECT_SOURCE_XPATH, SOURCE);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void selectSituation() throws Exception {
		final String SITUATION = "Avaliado";
		final String INFORMATION = " to select Situation as " + SITUATION;
		try {
			robot.xpathSelect(Constants.KNOWLEDGE_SELECT_SITUATION_XPATH, SITUATION);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void insertExpirationDate() throws Exception {
		final String EXPIRATION_DATE = "13/07/2018";
		final String INFORMATION = " to insert Expiration Date as " + EXPIRATION_DATE;
		try {
			robot.xpathInsert(Constants.KNOWLEDGE_INPUT_EXPIRATION_DATE_XPATH, EXPIRATION_DATE,
					createExpirationDateInsertionInterface());
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void selectPrivacy() throws Exception {
		final String PRIVACY = "Público";
		final String INFORMATION = " to select Privacy as " + PRIVACY;
		try {
			robot.xpathSelect(Constants.KNOWLEDGE_SELECT_PRIVACY_XPATH, PRIVACY);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void togglePublish() throws Exception {
		final String INFORMATION = " to toggle Publish";
		try {
			robot.xpathClick(Constants.KNOWLEDGE_TOGGLE_PUBLISH_XPATH);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void changeToSourceCode() throws Exception {
		final String INFORMATION = " to change to Source Code";
		try {
			robot.xpathClick(Constants.KNOWLEDGE_LINK_SOURCE_CODE_XPATH);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void insertProcedure(JSONObject data, JSONObject pop) throws Exception {
		final String INFORMATION = " to insert Procedure";
		try {
			final String PROCEDURE = buildProcedure(data, pop);
			final String JS = "arguments[0].value = \"" + PROCEDURE + "\";";
			robot.xpathExecuteJavaScript(Constants.KNOWLEDGE_TEXTAREA_XPATH, JS);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void submit() throws Exception {
		final String INFORMATION = " to click in Submit Button";
		try {
			if (robot.xpathExists(Constants.KNOWLEDGE_SAVE_DIV)) {
				robot.moveToCurrent();
				robot.moveToCurrent();
				robot.moveToCurrent();
				robot.moveToCurrent();
				if (robot.xpathExists(Constants.KNOWLEDGE_SAVE_ICON_DIV)) {
					robot.xpathClickOnChild(Constants.KNOWLEDGE_SAVE_BUTTON);
				}
			}
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private String buildProcedure(JSONObject data, JSONObject pop) {
		final String TEMPLATE = (String) ((JSONObject) data.get("template")).get("content");

		String procedure = TEMPLATE.replace("[[PROCEDURE]]", (String) pop.get("procedure"));
		procedure = procedure.replace("[[CATEGORY]]", (String) pop.get("category"));
		for (Object field : ((JSONObject) ((JSONObject) data.get("header")).get("fields")).keySet()) {
			String stringField = (String) field;
			final JSONObject popFields = (JSONObject) pop.get("fields");
			if (popFields.containsKey(stringField)) {
				procedure = procedure.replace("[[" + stringField + "]]", (String) popFields.get(stringField));
				procedure = procedure.replace("{{" + stringField + "}}",
						(String) ((JSONObject) ((JSONObject) data.get("header")).get("fields")).get(stringField));
			} else {
				procedure = procedure.replace("[[" + stringField + "]]", "");
				procedure = procedure.replace("{{" + stringField + "}}", "");
			}
		}

		return procedure;
	}

	private InsertionInterface createExpirationDateInsertionInterface() {
		return new InsertionInterface() {

			@Override
			public boolean beforeInsertion(Robot robot) throws Exception {
				robot.click();
				robot.click();
				return true;
			}

			@Override
			public boolean afterInsertion(Robot robot) throws Exception {
				if (robot.xpathExists(Constants.KNOWLEDGE_BUTTON_DATE_PICKER_CLOSE_XPATH)) {
					robot.click();
				}
				return true;
			}
		};
	}
}
