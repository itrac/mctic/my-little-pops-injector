package injector;

public class StateMachine {

	public enum States {
		SAFE, CAUTIOUS, UNSAFE, SEVERE, FAIL
	};

	private States state;

	public StateMachine() {
		state = States.SAFE;
	}

	public boolean isSafeToContinue() {
		return state == States.SAFE;
	}

	public boolean isPossibleToProceedWithCaution() {
		return state == States.SAFE || state == States.CAUTIOUS;
	}

	public boolean isNecessaryToRecoverToSafe() {
		return state == States.UNSAFE || state == States.SEVERE;
	}

	public boolean isNearToFailing() {
		return state == States.SEVERE;
	}

	public boolean isInjectionFailed() {
		return state == States.FAIL;
	}

	public void changeToSafe() {
		state = States.SAFE;
	}

	public void changeToCautious() {
		state = States.CAUTIOUS;
	}

	public void changeToUnsafe() {
		state = States.UNSAFE;
	}

	public void changeToSevere() {
		state = States.SEVERE;
	}

	public void changeToFail() {
		state = States.FAIL;
	}

	public void changeByAttempts(int attempts) {
		switch (attempts) {
		case 0:
			state = States.SAFE;
			break;
		case 1:
			state = States.CAUTIOUS;
			break;
		case 2:
			state = States.UNSAFE;
			break;
		case 3:
			state = States.SEVERE;
			break;
		default:
			state = States.FAIL;
		}
	}

}
