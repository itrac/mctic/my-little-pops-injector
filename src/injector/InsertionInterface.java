package injector;

public interface InsertionInterface {
	
	boolean beforeInsertion(Robot robot) throws Exception;
	boolean afterInsertion(Robot robot) throws Exception;
}
