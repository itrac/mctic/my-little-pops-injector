package injector;

public class Main {

	public static void main(String[] args) {
		Injector injector = new Injector();
		injector.inject();
		injector.report();
		injector.shutdown();
	}

}
