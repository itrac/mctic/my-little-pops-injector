package injector;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	private StateMachine stateMachine;

	private PrintWriter error;
	private PrintWriter inform;
	private PrintWriter result;
	private PrintWriter success;

	public Logger(StateMachine stateMachine) {
		this.stateMachine = stateMachine;
		try {
			createFiles();
		} catch (Exception e) {
			System.out.println("Logger Constructor EXCEPTION");
			System.out.println(e.getMessage());
			closeFiles();
			stateMachine.changeToFail();
		}
	}

	public void closeFiles() {
		try {
			if (error != null) {
				error.close();
			}
			if (inform != null) {
				inform.close();
			}
			if (result != null) {
				result.close();
			}
			if (success != null) {
				success.close();
			}
		} catch (Exception e) {
			System.out.println("Logger closeFiles EXCEPTION");
			System.out.println(e.getMessage());
			stateMachine.changeToFail();
		}

	}

	public void error(String context, String information) {
		log(context, information, "error");
	}

	public void inform(String context, String information) {
		log(context, information, "inform");
	}

	public void result(String context, String information) {
		log(context, information, "result");
	}

	public void success(String context, String information) {
		log(context, information, "success");
	}

	private void createFiles() {
		String timeStamp = getCurrentTimeStamp();
		createFile(timeStamp, "error");
		createFile(timeStamp, "inform");
		createFile(timeStamp, "result");
		createFile(timeStamp, "success");
	}

	private String buildFilePath(String timeStamp, String type) {
		return "/home/arkye/Workspace/injector/logs/" + timeStamp + "_" + type;
	}

	private String getCurrentTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private void log(String context, String information, String type) {
		try {
			String info = "[" + context + "]: " + information;
			switch (type) {
			case "error":
				error.println(info);
				break;
			case "inform":
				inform.println(info);
				break;
			case "result":
				result.println(information);
				inform.println(info);
				break;
			case "success":
				success.println(info);
				break;
			default:
				throw new Exception("Unknown type");
			}
			System.out.println(type + ": " + info);
		} catch (Exception e) {
			System.out.println("Logger log EXCEPTION");
			System.out.println(e.getMessage());
			stateMachine.changeToFail();
		}
	}

	private void createFile(String timeStamp, String type) {
		try {
			switch (type) {
			case "error":
				error = new PrintWriter(buildFilePath(timeStamp, type) + ".log", "utf-8");
				break;
			case "inform":
				inform = new PrintWriter(buildFilePath(timeStamp, type) + ".log", "utf-8");
				break;
			case "result":
				result = new PrintWriter(buildFilePath(timeStamp, type) + ".json", "utf-8");
				break;
			case "success":
				success = new PrintWriter(buildFilePath(timeStamp, type) + ".log", "utf-8");
				break;
			default:
				throw new Exception("Unknown type");
			}
		} catch (Exception e) {
			System.out.println("Logger createFile EXCEPTION");
			System.out.println(e.getMessage());
			stateMachine.changeToFail();
		}

	}
}
