package injector;

public class LoginManager {

	private final String CONTEXT;

	private Logger logger;
	private Robot robot;

	public LoginManager(Logger logger, Robot robot) {
		CONTEXT = "Login";

		this.logger = logger;
		this.robot = robot;
	}

	public void performLogin() throws Exception {
		final String INFORMATION = " to Login";
		try {
			openPage();
			if (robot.xpathExists(Constants.LOGIN_FORM_XPATH)) {
				insertUsername();
				insertPassword();
				submit();
				if (robot.xpathExists(Constants.PORTAL_INPUT_SEARCH)) {
					logger.success(CONTEXT, "Succeeded" + INFORMATION);
				} else {
					throw new Exception("After Login, Portal not found");
				}
			} else {
				throw new Exception("Login form not found");
			}
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void openPage() throws Exception {
		final String INFORMATION = " to open Login Page";
		try {
			if (Constants.IS_PRODUCTION) {
				robot.open(Constants.LOGIN_PRODUCTION_URL);
			} else {
				robot.open(Constants.LOGIN_DEVELOPMENT_URL);
			}
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void insertUsername() throws Exception {
		final String INFORMATION = " to Insert Username";
		try {
			robot.xpathInsert(Constants.LOGIN_INPUT_USERNAME_XPATH, Constants.LOGIN_INPUT_USERNAME);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void insertPassword() throws Exception {
		final String INFORMATION = " to Insert Password";
		try {
			robot.xpathInsert(Constants.LOGIN_INPUT_PASSWORD_XPATH, Constants.LOGIN_INPUT_PASSWORD);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}

	private void submit() throws Exception {
		final String INFORMATION = " to click in Submit Button";
		try {
			robot.xpathClick(Constants.LOGIN_BUTTON_XPATH);
			logger.inform(CONTEXT, "Succeeded" + INFORMATION);
		} catch (Exception e) {
			logger.error(CONTEXT, "Failed" + INFORMATION);
			throw e;
		}
	}
}
